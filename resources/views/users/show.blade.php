@extends('layouts.app')

@section('title', 'Users')

@section('content')

@if(Session::has('notdep'))
<div class = 'alert alert-danger'>
    {{Session::get('notdep')}}
</div>
@endif

<h1>user's page</h1>


<table class = "table table-dark">

    <!-- the table data -->

            <tr><th>id</th><th>{{$user->id}}</th></tr>
            <tr><th>name</th><th>{{$user->name}}</th></tr>
            <tr><th>email</th><th>{{$user->email}}</th></tr>
            <tr><th>role</th><th>@foreach($user->roles as $role)
                    {{ $role->name }}</th></tr>
                @endforeach
            </table>

            @can('admin-user')

    


                <form method = "post" action = "{{action('UsersController@update',$user->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group row">
                    <label for="department_id" class="col-md-4 col-form-label text-md-right">Move to department</label>
                    <div class="col-md-6">
                    
                        <select class="form-control" name="department_id">                                                                         
                          @foreach ($departments as $department)
                          <option value="{{ $department->id }}"> 
                              {{ $department->name }} 
                          </option>
                          @endforeach    
                        </select>
                    </div>


        <div>
            <input type = "submit" name = "submit" value = "Update candidate">
        </div>                       
        </form>    
            @endcan

            @cannot('admin-user')
            <tr><th>department</th><th>{{$user->department->name}}</th></tr> 
            @endcannot


@endsection

@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if(Session::has('notdelete'))
<div class = 'alert alert-danger'>
    {{Session::get('notdelete')}}
</div>
@endif
<h1>List of users</h1>


<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>department</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->department->name}}</td>  
            <td>
                <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>  
            <td>
                <a href = "{{route('users.show',$user->id)}}">Page</a>
            </td> 
            <td>
                <a href = "{{route('roles.manager',$user->id)}}">make manager</a>
            </td> 

        </tr>
    @endforeach
</table>
@endsection


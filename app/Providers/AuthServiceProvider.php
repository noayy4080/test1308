<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('change-status', function ($user, $candidate) {
            return $user->id === $candidate->user_id;
        });

        Gate::define('assign-user', function ($user) {
            return $user->isManager();
        });        

        Gate::define('delete-user', function ($user) {
            return $user->isAdmin();
        });  

        Gate::define('admin-user', function ($user) {
            return $user->isAdmin();
        });  

        Gate::define('user-can', function ($user) {
            if ($user->myCan()==null){
            return true;
            }
        });  

    }
}

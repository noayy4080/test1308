<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Candidate extends Model
{
    protected $fillable = ['name','email'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }     

    public static function myCan($uid){
        $can = DB::table('candidates')->where('user_id',$uid)->pluck('id');
        return self::find($can)->all(); 
    }

}
